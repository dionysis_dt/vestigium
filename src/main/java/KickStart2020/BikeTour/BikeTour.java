package KickStart2020.BikeTour;

import java.util.ArrayList;
import java.util.Scanner;

public class BikeTour {

    private int T;
    private ArrayList<int[]> pointSequences = new ArrayList<>();

    public static void main(String[] args) {

        BikeTour bikeTour = new BikeTour();
        bikeTour.readInput();

        for (int i=0; i<bikeTour.pointSequences.size(); i++) {
            System.out.println(String.format("Case #%d: %d", i+1, bikeTour.findPeaks(i)));
        }

    }

    private int findPeaks(int i) {
        int numPeaks = 0;

        int[] pointSequence = this.pointSequences.get(i);

        for(int j=1; j<pointSequence.length-1; j++) {
            if(pointSequence[j-1]<pointSequence[j] && pointSequence[j]>pointSequence[j+1]) {
                numPeaks++;
            }
        }

        return numPeaks;
    }

    private void readInput() {

        Scanner reader = new Scanner(System.in);
        String data;

        if (reader.hasNextLine()) {
            data = reader.nextLine();
            T = Integer.parseInt(data);
        }

        for (int i = 0; i < T; i++) {

            data = reader.nextLine();
            int N = Integer.parseInt(data);
            int[] pointSequence = new int[N];

            data = reader.nextLine();
            String[] cells = data.split(" ", 0);

            for (int j = 0; j < N; j++) {
                pointSequence[j] = Integer.parseInt(cells[j]);
            }

            pointSequences.add(pointSequence);
        }

        reader.close();

    }
}
