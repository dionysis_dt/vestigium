package KickStart2020.BusRoutes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class BusRoutes {

    private int T;
    private int[] D;
    private ArrayList<int[]> schedules = new ArrayList<>();

    public static void main(String[] args) {

        BusRoutes busRoutes = new BusRoutes();
        busRoutes.readInput();

        for (int i = 0; i< busRoutes.schedules.size(); i++) {
            System.out.println(String.format("Case #%d: %d", i+1, busRoutes.findLastDay(i)));
        }

    }

    private int findLastDay(int i) {
        int lastDay = this.findMaxLastDay(i);

        int[] schedule = this.schedules.get(i);

        for(int k=0; k<schedule.length; k++) {
//            while (lastDay % schedule[k] != 0) {
//                lastDay--;
//            }
        }

        return lastDay;
    }

    private int findMaxLastDay(int i) {
        int lastDay = this.D[i];

        int[] schedule = this.schedules.get(i);
        int[] lastDays = new int[schedule.length];

        for(int k=0; k<schedule.length; k++) {
            for(int j=0; j<=this.D[i]; j++) {
                if(j % schedule[k] == 0){
                    lastDays[k] = j;
                }
            }
        }

        for(int j=0; j<lastDays.length; j++) {
            if(lastDays[j]<lastDay) {
                lastDay=lastDays[j];
            }
        }

        return lastDay;
    }

    private void readInput() {

        Scanner reader = new Scanner(System.in);
        String data;

        if (reader.hasNextLine()) {
            data = reader.nextLine();
            T = Integer.parseInt(data);

            D = new int[T];
        }

        for (int i = 0; i < T; i++) {

            data = reader.nextLine();
            String[] input = data.split(" ", 0);
            int N = Integer.parseInt(input[0]);
            D[i] = Integer.parseInt(input[1]);

            int[] schedule = new int[N];

            data = reader.nextLine();
            String[] cells = data.split(" ", 0);

            for (int j = 0; j < N; j++) {
                schedule[j] = Integer.parseInt(cells[j]);
            }

            schedules.add(schedule);
        }

        reader.close();

    }
}
