package CodeJam2020.Round1.Indicium;

import java.util.*;

public class Indicium {

    private int T;
    private int trace;
    private ArrayList<int[][]> tables = new ArrayList<>();

    public static void main(String[] args) {

        Indicium indicium = new Indicium();
        indicium.readInput();

        for (int x=0; x<indicium.tables.size(); x++) {
            int[][] table = indicium.tables.get(x);
            indicium.populateTable(table);

            System.out.println(String.format("Case #%d: ", x+1));
            System.out.println(Arrays.deepToString(table));
        }

    }

    private void populateTable(int[][] table) {
        for(int i=0; i<table.length; i++) {
            for(int j=0; j<table[i].length; j++) {
                table[i][j] = i+j;
            }
        }
    }

    private void readInput() {

        //File input = Paths.get(getClass().getClassLoader().getResource("input1.txt").toURI()).toFile();
        Scanner reader = new Scanner(System.in);
        String data;

        if (reader.hasNextLine()) {
            data = reader.nextLine();
            T = Integer.parseInt(data);
        }

        for (int i = 0; i < T; i++) {

            data = reader.nextLine();
            String cells[] = data.split(" ", 0);
            int N = Integer.parseInt(cells[0]);
            trace = Integer.parseInt(cells[1]);

            int[][] table = new int[N][N];

            tables.add(table);
        }

        reader.close();

    }

    private int calcTrace(int[][] table) {

        int trace = 0;

        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                if (i == j) {
                    trace += table[i][j];
                } else if (j > i) {
                    break;
                }
            }
        }

        //System.out.println(String.format("Table trace:\t\t%d", trace));
        return trace;
    }

    private int calcDuplRows(int[][] table) {
        int duplRows = 0;

        for (int i = 0; i < table.length; i++) {
            Set<Integer> rowSet = new HashSet<>();
            for (int j = 0; j < table[i].length; j++) {
                rowSet.add(table[i][j]);
            }
            if(rowSet.size()<table[i].length) duplRows++;
        }

        //System.out.println(String.format("Duplicate rows:\t\t%d", duplRows));
        return duplRows;
    }

    private int calcDuplColumns(int[][] table) {
        int duplColumns = 0;

        for (int i = 0; i < table.length; i++) {
            Set<Integer> rowSet = new HashSet<>();
            for (int j = 0; j < table[i].length; j++) {
                rowSet.add(table[j][i]);
            }
            if(rowSet.size()<table[i].length) duplColumns++;
        }

        //System.out.println(String.format("Duplicate columns:\t%d", duplColumns));
        return duplColumns;
    }
}
