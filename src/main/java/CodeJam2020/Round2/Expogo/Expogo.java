package CodeJam2020.Round2.Expogo;

import java.util.*;

public class Expogo {

    private int T;
    private ArrayList<int[]> coordinates = new ArrayList<>();

    public static void main(String[] args) {

        Expogo expogo = new Expogo();
        expogo.readInput();

        for (int x = 0; x< expogo.coordinates.size(); x++) {
            int[] start = new int[2];
            start[0] = 0;
            start[1] = 0;

            System.out.println(String.format("Case #%d: %s", x+1, expogo.findPath(start, expogo.coordinates.get(x))));
            System.out.println("=======================================");
        }

    }

    private String findPath(int[] start, int[] end) {

        String path = "";

        if(end[0] % 2 != 0 && end[1] % 2 != 0) return "IMPOSSIBLE";
        else if(end[0] % 2 == 0 && end[1] % 2 == 0) return "IMPOSSIBLE";
        else {
            while (start[0] != end[0] || start[1] != end[1]) {
                path += nextStep(start, end);
            }
        }

        return path;
    }

    private String nextStep(int[] start, int[] end) {
        String step = "Y";

        start[0] = end[0];
        start[1] = end[1];

        String xAxis = Integer.toBinaryString(Math.abs(end[0]));
        System.out.println(xAxis);
        String yAxis = Integer.toBinaryString(Math.abs(end[1]));
        System.out.println(yAxis);

        int steps = 0;
        for(char c : xAxis.toCharArray()) {
            if(c=='1') steps++;
        }
        for(char c : yAxis.toCharArray()) {
            if(c=='1') steps++;
        }

        System.out.println(steps);

        return step;
    }

    private void block(int x)
    {
        ArrayList<Integer> v = new ArrayList<Integer>();

        // Convert decimal number to
        // its binary equivalent
        System.out.print("Blocks for "+x+" : ");
        while (x > 0)
        {
            v.add((int)x % 2);
            x = x / 2;
        }

        // Displaying the output when
        // the bit is '1' in binary
        // equivalent of number.
        for (int i = 0; i < v.size(); i++)
        {
            if (v.get(i) == 1)
            {
                System.out.print(i);
                if (i != v.size() - 1)
                    System.out.print( ", ");
            }
        }
        System.out.println();
    }

    private void readInput() {

        Scanner reader = new Scanner(System.in);
        String data;

        if (reader.hasNextLine()) {
            data = reader.nextLine();
            T = Integer.parseInt(data);
        }

        for (int i = 0; i < T; i++) {

            data = reader.nextLine();
            String cells[] = data.split(" ", 0);

            int[] coordinate = new int[2];
            coordinate[0] = Integer.parseInt(cells[0]);
            coordinate[1] = Integer.parseInt(cells[1]);

            coordinates.add(coordinate);
        }

        reader.close();

    }

}
