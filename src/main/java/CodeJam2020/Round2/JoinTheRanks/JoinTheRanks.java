package CodeJam2020.Round2.JoinTheRanks;

import java.util.ArrayList;
import java.util.Scanner;

public class JoinTheRanks {

    private int T;
    private ArrayList<int[]> coordinates = new ArrayList<>();

    public static void main(String[] args) {

        JoinTheRanks joinTheRanks = new JoinTheRanks();
        joinTheRanks.readInput();

        for (int x = 0; x< joinTheRanks.coordinates.size(); x++) {
            int[] start = new int[2];
            start[0] = 0;
            start[1] = 0;

            System.out.println(String.format("Case #%d: ", x+1));
            System.out.println("=======================================");
        }

    }

      private void readInput() {

        Scanner reader = new Scanner(System.in);
        String data;

        if (reader.hasNextLine()) {
            data = reader.nextLine();
            T = Integer.parseInt(data);
        }

        for (int i = 0; i < T; i++) {

            data = reader.nextLine();
            String cells[] = data.split(" ", 0);

            int[] coordinate = new int[2];
            coordinate[0] = Integer.parseInt(cells[0]);
            coordinate[1] = Integer.parseInt(cells[1]);

            coordinates.add(coordinate);
        }

        reader.close();

    }

}
