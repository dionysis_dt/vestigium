package CodeJam2022.Qualification.d1000000;

import java.util.*;
import java.util.stream.Collectors;

public class D1000000 {

    private int T;
    private HashMap<Integer, Integer> diceNumbers = new HashMap<>();
    private ArrayList<HashMap<Integer, Integer>> maps = new ArrayList<>();

    public static void main(String[] args) {

        D1000000 d1000000 = new D1000000();
        d1000000.readInput();

        for (int x = 0; x< d1000000.maps.size(); x++) {
            int N = d1000000.diceNumbers.get(x);
            HashMap<Integer, Integer> map = d1000000.maps.get(x);

            int maxStraight = d1000000.solveTestCase(N, map);
            System.out.println(String.format("Case #%d: %d", x+1, maxStraight));
        }

    }

    private int solveTestCase(int diceNumber, HashMap<Integer, Integer> map) {

        int maxStraightPossible = Math.min(diceNumber, findMaxMapKey(map));

        int maxStraight = 0;
        List<Integer> diceValues = map.keySet().stream().sorted().collect(Collectors.toList());
        for(int mapValue : diceValues) {
            for(int j=0; j<map.get(mapValue); j++) {
                if (maxStraight < mapValue) {
                    maxStraight++;
                } else {
                    continue;
                }
                if(maxStraight==maxStraightPossible) break;
            }

            if(maxStraight==maxStraightPossible) break;
        }

        return maxStraight;
    }

    private int findMaxMapKey(HashMap<Integer, Integer> map) {
        int maxDiceValue = 0;
        for (int i : map.keySet()) {
            if (i>maxDiceValue) {
                maxDiceValue = i;
            }
        }
        return maxDiceValue;
    }

    private void readInput() {

        Scanner reader = new Scanner(System.in);
        String data;

        if (reader.hasNextLine()) {
            data = reader.nextLine();
            T = Integer.parseInt(data);
        }

        for (int i = 0; i < T; i++) {
            data = reader.nextLine();
            int N = Integer.parseInt(data);
            diceNumbers.put(i, N);

            HashMap<Integer, Integer> map = new HashMap();
            data = reader.nextLine();
            String[] cells = data.split(" ", 0);

            for(String cell : cells) {
                Integer number = Integer.parseInt(cell);
                if(map.containsKey(number)) {
                    map.replace(number, map.get(number)+1);
                } else {
                    map.put(number, 1);
                }
            }

            maps.add(map);
        }

        reader.close();

    }

}