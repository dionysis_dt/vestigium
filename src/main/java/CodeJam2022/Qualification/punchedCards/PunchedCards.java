package CodeJam2022.Qualification.punchedCards;

import java.util.*;

public class PunchedCards {

    private int N;
    private ArrayList<int[]> tables = new ArrayList<>();

    public static void main(String[] args) {

        PunchedCards punchedCards = new PunchedCards();
        punchedCards.readInput();

        for (int x=0; x<punchedCards.tables.size(); x++) {
            int[] table = punchedCards.tables.get(x);

            System.out.println(String.format("Case #%d:", x+1));
            punchedCards.printPunchCard(table);
        }

    }

    private void printPunchCard(int[] dimensions) {
        int R = dimensions[0];
        int C = dimensions[1];

        String firstRowHeader = "..+";
        String rowHeaderPattern = "-+";
        firstRowHeader += rowHeaderPattern.repeat(C-1);
        System.out.println(firstRowHeader);

        String firstRow = "..|";
        String rowPattern = ".|";
        firstRow += rowPattern.repeat(C-1);
        System.out.println(firstRow);

        String rowFooter = "+-+";
        String rowFooterPattern = "-+";
        rowFooter += rowFooterPattern.repeat(C-1);
        System.out.println(rowFooter);

        String restRows = "|.|";
        restRows += rowPattern.repeat(C-1);

        for (int i = 0; i < R-1; i++) {
            System.out.println(restRows);
            System.out.println(rowFooter);
        }

    }
    private void readInput() {

        Scanner reader = new Scanner(System.in);
        String data;

        if (reader.hasNextLine()) {
            data = reader.nextLine();
            N = Integer.parseInt(data);
        }

        for (int i = 0; i < N; i++) {

            data = reader.nextLine();
            int[] table = new int[2];

            String[] cells = data.split(" ", 0);
            table[0] = Integer.parseInt(cells[0]);
            table[1] = Integer.parseInt(cells[1]);


            tables.add(table);
        }

        reader.close();

    }

}