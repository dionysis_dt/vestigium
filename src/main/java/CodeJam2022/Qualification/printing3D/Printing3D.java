package CodeJam2022.Qualification.printing3D;

import java.util.ArrayList;
import java.util.Scanner;

public class Printing3D {

    private int N;
    private ArrayList<int[][]> tables = new ArrayList<>();

    public static void main(String[] args) {

        Printing3D printing3D = new Printing3D();
        printing3D.readInput();

        for (int x = 0; x< printing3D.tables.size(); x++) {
            int[][] table = printing3D.tables.get(x);

            printing3D.solveTestCase(x, table);
        }

    }

    private void solveTestCase(int x, int[][] cartridges) {

        int[] CMYK = minimumCMYK(cartridges);
        boolean isPossible = isPossible(CMYK);

        if(isPossible) {
            calculateCMYK(CMYK);
            System.out.println(String.format("Case #%d: %d %d %d %d", x+1, CMYK[0], CMYK[1], CMYK[2], CMYK[3]));
        } else {
            System.out.println(String.format("Case #%d: IMPOSSIBLE", x+1));
        }
    }

    private void calculateCMYK(int[] CMYK) {
        int remainingInkUnits = 1000000;

        for(int i=0; i<CMYK.length; i++) {
            if(remainingInkUnits <= CMYK[i]) {
                CMYK[i] = remainingInkUnits;
            } else {
                CMYK[i] = CMYK[i];
            }

            remainingInkUnits -= CMYK[i];
        }
    }

    private int[] minimumCMYK(int[][] cartridges) {
        int[] CMYK = new int[4];
        CMYK[0] = 1000000;
        CMYK[1] = 1000000;
        CMYK[2] = 1000000;
        CMYK[3] = 1000000;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++){
                if(cartridges[i][j] <= CMYK[j]) {
                    CMYK[j] = cartridges[i][j];
                }
            }
        }

        return CMYK;
    }

    private boolean isPossible(int[] CMYK) {
        boolean isPossible;

        int totalInkUnits = 0;
        for (int i = 0; i < CMYK.length; i++) {
            totalInkUnits += CMYK[i];
        }

        isPossible = (totalInkUnits >= 1000000);
        return isPossible;
    }

    private void readInput() {

        Scanner reader = new Scanner(System.in);
        String data;

        if (reader.hasNextLine()) {
            data = reader.nextLine();
            N = Integer.parseInt(data);
        }

        for (int i = 0; i < N; i++) {
            int[][] table = new int[N][4];

            for (int j = 0; j < 3; j++) {
                data = reader.nextLine();

                String[] cells = data.split(" ", 0);
                table[j][0] = Integer.parseInt(cells[0]);
                table[j][1] = Integer.parseInt(cells[1]);
                table[j][2] = Integer.parseInt(cells[2]);
                table[j][3] = Integer.parseInt(cells[3]);
            }

            tables.add(table);
        }

        reader.close();

    }

}