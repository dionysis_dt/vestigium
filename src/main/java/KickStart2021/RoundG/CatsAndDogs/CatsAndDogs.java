package KickStart2021.RoundG.CatsAndDogs;

import java.util.ArrayList;
import java.util.Scanner;

public class CatsAndDogs {

    private static int T;

    private ArrayList<Integer> N = new ArrayList<Integer>();
    private ArrayList<Integer> D = new ArrayList<Integer>();
    private ArrayList<Integer> C = new ArrayList<Integer>();
    private ArrayList<Integer> M = new ArrayList<Integer>();

    private ArrayList<String> S = new ArrayList<String>();

    public static void main(String[] args) {

        CatsAndDogs catsAndDogs = new CatsAndDogs();
        catsAndDogs.readInput();

        for (int i = 0; i < catsAndDogs.N.size(); i++) {
            System.out.println(String.format("Case #%d: %s", i + 1, catsAndDogs.findPeaks(i)));
        }

    }

    private String findPeaks(int i) {

        int numberOfAnimals = N.get(i);
        int numberOfDogPortions = D.get(i);
        int numberOfCatPortions = C.get(i);
        int numberOfAddedCatPortions = M.get(i);

        String animalSequence = S.get(i);

        char[] animalSequence2 = new char[animalSequence.length()];
        animalSequence.getChars(0,animalSequence.length(), animalSequence2, 0);

        for (int j=0;j<animalSequence.length();j++) {
            if(animalSequence2[j]=='D') {
                if(numberOfDogPortions==0) return "NO";

                numberOfDogPortions--;
                numberOfCatPortions+=numberOfAddedCatPortions;
            } else {
                if(numberOfCatPortions==0 & animalSequence.substring(j+1).contains("D")) return "NO";
                numberOfCatPortions--;
            }
        }

        return "YES";
    }

    private void readInput() {

        Scanner reader = new Scanner(System.in);

        String data = reader.nextLine();

        T = Integer.parseInt(data);

        for (int i = 0; i < T; i++) {

            data = reader.nextLine();

            String[] args = data.split(" ", 0);
            int n = Integer.parseInt(args[0]);
            N.add(n);

            int d = Integer.parseInt(args[1]);
            D.add(d);

            int c = Integer.parseInt(args[2]);
            C.add(c);

            int m = Integer.parseInt(args[3]);
            M.add(m);


            data = reader.nextLine();

            String animalSequence = data;
            S.add(animalSequence);

        }

        reader.close();

    }
}
